import { Component } from '@angular/core';
import { Video } from '../interfaces';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent {
  selectedVideo?: Video;
  videos: Video[] = [];

  constructor(http: HttpClient) {
    http
      .get<Video[]>('https://api.angularbootcamp.com/videos')
      .subscribe(videos => {
        this.videos = videos;
        this.selectedVideo = videos[0];
     });
  }

}
